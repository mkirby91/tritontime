<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="users.*, commands.*, timecard.* "%>
<%@ page import="java.util.List"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet"
	href="tritontime/jquery/jquery-ui-1.10.3.custom/css/excite-bike/jquery-ui-1.10.3.custom.min.css">
<script type="text/javascript"
	src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
<script type="text/javascript"
	src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="tritontime/login.js"></script>
</head>

<body>
	<h4>You have successfully logged out.</h4>

	<form method="post" action="TritonTime.jsp">
		<p class="remember">
			<input value="Log Back In" tabindex="6"
				type="submit" class="ui-corner-all ui-state-default">
		</p>
		<input type="hidden" name="action" value="logout" />
	</form>
</body>
</html>