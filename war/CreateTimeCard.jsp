<%@ page import="users.*, commands.*, timecard.*, persistence.*, java.util.ArrayList"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!doctype html>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="tritontime/jquery/jquery-ui-1.10.3.custom/css/excite-bike/jquery-ui-1.10.3.custom.min.css">
    
   
    <title>TritonTime - Create Timecard</title>
    
    <!--                                           -->
    <!-- This script loads your compiled module.   -->
    <!-- If you add any GWT meta tags, they must   -->
    <!-- be added before this line.                -->
    <!--                                           -->
    <script type="text/javascript" src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
    


<script type="text/javascript">
        $(document).ready(function() {

        });
</script>

<script>
var availableDates = ["1-6-2013","15-6-2013","29-6-2013"];

function available(date) {
  dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
  if ($.inArray(dmy, availableDates) !== -1) {
    return [true, "","Closing Day"];
  } else {
    return [false,"","Invalid"];
  }
}

  $(function() {
    $( "#datepicker" ).datepicker({ beforeShowDay: available });
  });
  </script>
  <!--                                           -->
  <!-- The body can have arbitrary html, or      -->
  <!-- you can leave the body empty if you want  -->
  <!-- to create a completely dynamic UI.        -->
  <!--                                           -->
  <div class=ui-widget>
    <h1 class="ui-widget-header">New Time Card</h1>

<div id="container" >

<form id= "commentForm" >
  <fieldset>
  	<p>Date: <input type="text" id="datepicker" name="date" /></p>
  	<input type="submit" value ="Submit"/>
  	<input type="hidden" name="action" value="datechosen" action="CreateTimeCard.jsp"/>
  </fieldset>
  </form>

 </div>
	<%
    	String act = request.getParameter("action");
		
    	if(act != null && act.equals("datechosen")){
    		//somehow update db entry and go back to previous page
			User user = User.getUser((String)session.getAttribute("userId"));
			
			Entries entries = new Entries("Timecard","",(String)request.getParameter("date"),user.getId(),"A111",new ArrayList<String>());
			user.executeCommand(CommandObserver.getCommandObserver("CommandObserver"),"Add",entries,entries.getParentIds());
			user.persist();
    		response.sendRedirect("CreateTimeCard.jsp");
    	}
    %>

  </body>
</html>
