<!doctype html>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="tritontime/jquery/jquery-ui-1.10.3.custom/css/excite-bike/jquery-ui-1.10.3.custom.min.css">
    
   
    <title>TritonTime - Update Timecard</title>
    
    <!--                                           -->
    <!-- This script loads your compiled module.   -->
    <!-- If you add any GWT meta tags, they must   -->
    <!-- be added before this line.                -->
    <!--                                           -->
    <script type="text/javascript" src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>


<script type="text/javascript">
        $(document).ready(function() {

        });
</script>

</head>
  <!--                                           -->
  <!-- The body can have arbitrary html, or      -->
  <!-- you can leave the body empty if you want  -->
  <!-- to create a completely dynamic UI.        -->
  <!--                                           -->
  <body class=ui-widget>
    <h1 class="ui-widget-header">Timecard</h1>

<div id="container" >
  <fieldset id="signin_menu" class="ui-widget-content ui-corner-all">
  <h4 class="ui-widget-header">Edit existing entry.</h4>
    <form method="post" id="signin" action="">
      <label for="dep">Department</label>
      <input id="dep" name="dep"value=""title="dep"tabindex="4"type="text" class="ui-corner-all">
      <p>
        <label for="Course">Course</label>
        <input id="course"name="course"value=""title="course"tabindex="5"type="course" class="ui-corner-all">
      </p>    
      <p>
        <label for="professor">Professor</label>
        <input id="professor"name="professor"value=""title="professor"tabindex="5"type="professor" class="ui-corner-all">
      </p>  
      <p class="remember">
        <input id="newentry_submit" value="Update" tabindex="6" type="submit" class="ui-corner-all ui-state-default">
        
      </p>
      
  	<input type="hidden" name="action" value="submitted"/>
      
    </form>
  </fieldset>
  
  </div>

	<%
    	String act = request.getParameter("action");
    
    	if(act != null && act.equals("sumbitted")){
    		//somehow update db entry and go back to previous page
    		response.sendRedirect("UpdateTimeCard.jsp");
    	}
    %>
  </div>

  </body>
</html>
