
<div id="navigation" class="ui-widget-header ui-corner-all" style="float:right">
<span id ="toolbar">
	<label>
		<%=session.getAttribute("userRole")%>: <%=session.getAttribute("userName")%>
	</label>
	<form method="post" action="Logout.jsp">
			<input value="Log Out" tabindex="6" type="submit" class="ui-corner-all ui-state-default">
	</form>
</span>
</div>

<%if(session.getAttribute("userRole").equals("Tutor")){ %>
<h2 class="ui-widget-header">TUTOR homepage</h2>
<%}else if(session.getAttribute("userRole").equals("Professor")){ %>
	<h2 class="ui-widget-header">PROFESSOR homepage</h2>
<%} else{ %>
	<h2 class="ui-widget-header">HR homepage</h2>
<%}%>