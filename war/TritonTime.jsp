<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="users.*, commands.*, timecard.*, persistence.* "%>
<%@ page import="javax.persistence.EntityManager"%>
<%@ page import="javax.persistence.EntityManagerFactory"%>
<%@ page import="javax.persistence.Query"%>
<%@ page import="javax.persistence.NoResultException"%>
<%@ page import="java.util.List"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet"
	href="tritontime/jquery/jquery-ui-1.10.3.custom/css/excite-bike/jquery-ui-1.10.3.custom.min.css">
<script type="text/javascript"
	src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
<script type="text/javascript"
	src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="tritontime/login.js"></script>

<title>TritonTime</title>

</head>
<!--                                           -->
<!-- The body can have arbitrary html, or      -->
<!-- you can leave the body empty if you want  -->
<!-- to create a completely dynamic UI.        -->
<!--                                           -->
<body class="ui-widget">
	<h1 class="ui-widget-header">Welcome to TritonTime!</h1>

	<% 
  	boolean notfound = false;
		boolean empty = false;
		
		String action = request.getParameter("action");
		
		 // Check if an insertion is requested
        if (action != null && action.equals("login")) {
			if (!(request.getParameter("username").equals(""))) {
				
				User theUser = User.getUser(request.getParameter("username"));
				if(theUser!=null){
					session.setAttribute("userId", theUser.getId());
					session.setAttribute("userRole", theUser.getRole());
					session.setAttribute("userName", theUser.getName());
				}
				else{
					notfound = true;
				}
			}else{
				empty = true;
			}
		}
	%>

	<div id="container">
		<fieldset class="ui-widget-content ui-corner-all">
			<h4 class="ui-widget-header">Sign on using your UCSD Personal ID
				number (PID) and Personal Access Code (PAC).</h4>

			<form method="post" id="signin" action="TritonTime.jsp">
				<label for="username">User ID / PID</label> <input id="username"
					name="username" value="" title="username" tabindex="4" type="text"
					class="ui-corner-all">
				<p>
					<label for="password">Password / PAC</label> <input id="password"
						name="password" value="" title="password" tabindex="5"
						type="password" class="ui-corner-all">
				</p>
				<p class="remember">
					<input value="Sign in" tabindex="6"
						type="submit" class="ui-corner-all ui-state-default">
				</p>
				<input type="hidden" name="action" value="login" />
			</form>
		</fieldset>
	</div>
	<% if (empty) { %>
	<strong style="color: red;">A name must be provided.</strong>
	<% } else {
		   if (notfound) { %>
	<strong style="color: red;">Username not found.</strong>
	<% 	   } else { 
   				action = request.getParameter("action");
   				if (action != null && action.equals("login")) {
		   	       response.sendRedirect("Homepage.jsp");
   				}
   			}
	}
	       
	    
	%>

</body>
</html>
