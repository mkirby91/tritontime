<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="users.*, commands.*, timecard.* "%>
<%@ page import="java.util.List"%>

<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet"
	href="tritontime/jquery/jquery-ui-1.10.3.custom/css/excite-bike/jquery-ui-1.10.3.custom.min.css">
<script type="text/javascript"
	src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
<script type="text/javascript"
	src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="tritontime/login.js"></script>
<link type="text/css" rel="stylesheet" href="PageLook.css">

<%-- -------- Include basic navigation HTML code -------- --%> 

<jsp:include page="UserNavigation.jsp" />
</head>


<body class="ui-widget">
<jsp:include page="TimeCard.jsp" />

</body>
</html>