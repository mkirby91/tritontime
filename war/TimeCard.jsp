<!doctype html>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="tritontime/jquery/jquery-ui-1.10.3.custom/css/excite-bike/jquery-ui-1.10.3.custom.min.css">
    <link type="text/css" rel="stylesheet" href="tritontime/jquery/DataTables-1.9.4/media/css/jquery.dataTables.css">
    
    <title>TritonTime - Timecard</title>
    
    <!--                                           -->
    <!-- This script loads your compiled module.   -->
    <!-- If you add any GWT meta tags, they must   -->
    <!-- be added before this line.                -->
    <!--                                           -->
    <script type="text/javascript" src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="tritontime/jquery/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="tritontime/jquery/DataTables-1.9.4/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="tritontime/jquery/customjquery.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	var trail = 0;
	var parents = new Array()
	var lastUrl = new Array();
	lastUrl[trail] = 'timecardsdata.jsp';
	$("#back").hide();
	 
    /* Init the table */
    oTable = $('#example').dataTable( {
        "bProcessing": true,
		"bJQueryUI": true,
		"oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries."},
        "aoColumns": [
					{ "sTitle": "ID"},
					{ "sTitle": "Pay Period" },
					{ "sTitle": "Hours" },
					{ "sTitle": "Status"},
					{ "sTitle": "Approver" },
					{ "sTitle": "Approver Comment" },
					{ "sTitle": "Actions"}                                                               
                                        ],
        "sAjaxSource": 'timecardsdata.jsp',
		"fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
			if (aaData[0]) {
				var type = aaData[aaData.length-1][0];
				switch (type) {
					case "entry":
						$("tr th:nth-child(1)").text("Date");
						$("tr th:nth-child(2)").text("Hours");
						$("tr th:nth-child(3)").text("Hours Type");
						$("tr th:nth-child(4)").text("Hours Description");
						var button = $('<button>').html("New Daily Entry");
						button.click(function(e) { 
							$( "#dialog-form" ).load("CreateDailyEntry.jsp");
							$( "#dialog-form" ).dialog({modal: true,width: 400,
		position: [($(window).width() / 2)-200, 150]}, "center");});
						$('#buttons').html( button );
						/*
						$("tr th:nth-child(5)").hide();
						$("tr th:nth-child(6)").hide();*/
					break;
					case "class":
						$("tr th:nth-child(1)").text("Department");
						$("tr th:nth-child(2)").text("Course");
						$("tr th:nth-child(3)").text("Hours");
						$("tr th:nth-child(4)").text("Status");
						$("tr th:nth-child(5)").text("Approver");
						$("tr th:nth-child(6)").text("Approver Comment");
						var button = $('<button>').html("New Class Entry");
						button.click(function(e) { 
							$( "#dialog-form" ).load("AddEntriesTimeCard.jsp?parents="+parents[0]);
							$( "#dialog-form" ).dialog({modal: true,width: 400,
		position: [($(window).width() / 2)-200, 150]}, "center");});
						$('#buttons').html( button );
					break;
					case "entryComp":
						$("tr th:nth-child(1)").text("ID");
						$("tr th:nth-child(2)").text("Pay Period");
						$("tr th:nth-child(3)").text("Hours");
						$("tr th:nth-child(4)").text("Status");
						$("tr th:nth-child(5)").text("Approver");
						$("tr th:nth-child(6)").text("Approver Comment");
						var button = $('<button>').html("New Timecard");
						button.click(function(e) { 
							$( "#dialog-form" ).load("CreateTimecard.jsp");
							$( "#dialog-form" ).dialog({modal: true,width: 400,
		position: [($(window).width() / 2)-200, 150]}, "center");});
						$('#buttons').html( button );
					break;
				}
			}
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			$('td:last-child', nRow).html('');
			for (var i=0;i<aData[aData.length-2].length;i++) {
				switch (aData[aData.length-2][i]) {
					case 1:
						var button = $('<button>').html("Approve");
						button.click(function(e) { 
							$( "#dialog-form" ).load("ReviewTimecard.jsp?n="+i);
							$( "#dialog-form" ).dialog({modal: true,width: 400,
		position: [($(window).width() / 2)-200, 150]}, "center");});
						$('td:last-child', nRow).append( button );
					break;
					case 2:
						var button = $('<button>').html("Update");
						button.click(function(e) { 
							$( "#dialog-form" ).load("EditTimecard.jsp?n="+i);
							$( "#dialog-form" ).dialog({modal: true,width: 400,
		position: [($(window).width() / 2)-200, 150]}, "center");});
						$('td:last-child', nRow).append( button );

					break;
					case 3:
						var button = $('<button>').html("Remove");
						button.click(function(e) { 
							$( "#dialog-form" ).load("DeleteTimecard.jsp?n="+i);
							$( "#dialog-form" ).dialog({modal: true,width: 400,
		position: [($(window).width() / 2)-200, 150]}, "center");});
						$('td:last-child', nRow).append( button );
					break;
				
				}
			}
			
				
			if (aData[1] == null) {
				$(nRow).hide();
			} else {
				$('td:first-child', nRow).click( function( e ) {
						switch (aData[aData.length-1]) {
							case 1: //Time card top level
								oTable.fnReloadAjax("timecardsdata.jsp?n="+aData[0]);
								parents[trail] = aData[0];
								trail++;
								$("#back").show();
								lastUrl[trail] = "timecardsdata.jsp?n="+aData[0];
							break;
							case 2: //Class entry
								oTable.fnReloadAjax("timecardsdata.jsp?t=CSE&n="+aData[0]);
								parents[trail] = aData[0];
								trail++;
								$("#back").show();
								lastUrl[trail] = "timecardsdata.jsp?t=CSE&n="+aData[0];
							break;
							case 3: //Entry
								alert("Edit popup window here!");
							break;
						}
						
				});
			}
		}
    } );
	
	$("#back").click(function theFunc() {
		if (trail >= 1) {
			trail--;
			oTable.fnReloadAjax(lastUrl[trail]);
			if (trail == 0) {
				$("#back").hide();
			}
		}
	});

} );
  </script>
  
  <div id="buttons">
  </div>
  <!--                                           -->
  <!-- The body can have arbitrary html, or      -->
  <!-- you can leave the body empty if you want  -->
  <!-- to create a completely dynamic UI.        -->
  <!--                                           -->
  <div class=ui-widget>
    <h1 class="ui-widget-header">New Time Card</h1>

<div id="container" >
<a href="#" id="back">Back</a>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>ID</th>
			<th>Pay Period</th>
			<th>Status</th>
            <th>Actions</th>
            <th>Actions</th>
            <th>Actions</th>
            <th>Actions</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
		<tr>
			<th>ID</th>
			<th>Pay Period</th>
			<th>Status</th>
            <th>Actions</th>
            <th>Actions</th>
            <th>Actions</th>
            <th>Actions</th>
		</tr>
	</tfoot>
</table>
  
  </div>
  
  <div style='display:none'>
	<div id="dialog-form" title="Review Timecard">
  		<p>Loading</p>
  		</div>
  		</div>

  </body>
</html>
