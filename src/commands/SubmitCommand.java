package commands;

import timecard.*;
import users.Approver;
import users.Tutor;
import users.User;
import java.util.Iterator;
import java.util.ArrayList;

public class SubmitCommand extends Command
{
	public SubmitCommand(CommandObserver co, String name, EntryComponent ec, ArrayList<String> parentIds)
	{
		super(co, name, null, ec, parentIds);
	}

	public void execute()
	{
		Entries submission = (Entries)this.getEc();
		if(submission.getType().equals("Timecard"))
		{
			Iterator<EntryComponent> iter = submission.iterator();
			Entries classEntry;
			while(iter.hasNext())
			{
				classEntry = (Entries)iter.next();
				classEntry.setComment("");
				classEntry.setStatus("Submitted");
				Approver approver = (Approver)User.getUser(classEntry.getApproverId());
				approver.executeCommand(this.getCo(), "Add", classEntry, new ArrayList<String>());
				approver.persist();
			}
			submission.setStatus("Submitted");
			Tutor tutor = (Tutor)User.getUser(submission.getTutorId());
			tutor.executeCommand(this.getCo(), "Remove", submission, new ArrayList<String>());
			ArrayList<String> submittedId = new ArrayList<String>();
			submittedId.add("Submitted" + tutor.getId());
			tutor.executeCommand(this.getCo(), "Add", submission, submittedId);
			tutor.persist();
			// setChanged();
			// notifyObservers(submission);
		}
		else if(submission.getType().equals("ClassEntry"))
		{
			submission.setComment("");
			submission.setStatus("Submitted");
			User approver = User.getUser(submission.getApproverId());
			approver.executeCommand(this.getCo(), "Add", submission, new ArrayList<String>());
			User tutor = User.getUser(submission.getTutorId());
			ArrayList<String> disapprovedId = new ArrayList<String>();
			disapprovedId.add("Disapproved" + tutor.getId());
			tutor.executeCommand(this.getCo(), "Remove", submission, disapprovedId);
			approver.persist();
			tutor.persist();
			// setChanged();
			// notifyObservers(submission);
		}
	}
}
