package commands;

import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.Test;
import timecard.*;
import users.*;

public class CommandObserverTest
{
	@Test
	public void testUpdateSubmit()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Approver professor2  = new Approver("333", "Jim Professor", "Professor", "jprofessor@ucsd.edu", "CSE");
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		Entries classEntry2 = new Entries("ClassEntry", "Class 2", "3/24/1987", tutor, professor2, classEntryParentIds);
		tutor.executeCommand(co, "Add", timecard, timecard.getParentIds());
		tutor.executeCommand(co, "Add", classEntry, classEntryParentIds);
		tutor.executeCommand(co, "Add", classEntry2, classEntryParentIds);
		tutor.executeCommand(co, "Submit", timecard, timecard.getParentIds());
		
		assertNull(tutor.getEntries().get(timecard.getId()));
		assertNotNull(co.getPendingTimecards().get(timecard.getId()));
		assertEquals(timecard, co.getPendingTimecards().get(timecard.getId()));
	}
	
	@Test
	public void testUpdateApproveClassEntryPending()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Approver professor2  = new Approver("333", "Jim Professor", "Professor", "jprofessor@ucsd.edu", "CSE");
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		Entries classEntry2 = new Entries("ClassEntry", "Class 2", "3/24/1987", tutor, professor2, classEntryParentIds);
		tutor.executeCommand(co, "Add", timecard, timecard.getParentIds());
		tutor.executeCommand(co, "Add", classEntry, classEntryParentIds);
		tutor.executeCommand(co, "Add", classEntry2, classEntryParentIds);
		tutor.executeCommand(co, "Submit", timecard, timecard.getParentIds());
		professor.executeCommand(co, "Approve ClassEntry", "Comment: Approved", classEntry, new ArrayList<String>());
		
		assertNull(tutor.getEntries().get(timecard.getId()));
		assertNotNull(co.getPendingTimecards().get(timecard.getId()));
	}
	
	@Test
	public void testUpdateApproveClassEntryFinal()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Approver professor2  = new Approver("333", "Jim Professor", "Professor", "jprofessor@ucsd.edu", "CSE");
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		Entries classEntry2 = new Entries("ClassEntry", "Class 2", "3/24/1987", tutor, professor2, classEntryParentIds);
		tutor.executeCommand(co, "Add", timecard, timecard.getParentIds());
		tutor.executeCommand(co, "Add", classEntry, classEntryParentIds);
		tutor.executeCommand(co, "Add", classEntry2, classEntryParentIds);
		tutor.executeCommand(co, "Submit", timecard, timecard.getParentIds());
		professor.executeCommand(co, "Approve ClassEntry", "Comment: Approved", classEntry, new ArrayList<String>());
		professor2.executeCommand(co, "Approve ClassEntry", "Comment: Approved", classEntry2, new ArrayList<String>());
		
		assertNull(tutor.getEntries().get(timecard.getId()));
		assertNull(co.getPendingTimecards().get(timecard.getId()));
		assertNotNull(hrapprover.getEntries().get(timecard.getId()));
	}
}
