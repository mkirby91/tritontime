package commands;

import timecard.*;
import users.User;

import java.util.ArrayList;

public class ApprovalCommand extends Command
{
	private String approval;
	private String comment;
	
	public ApprovalCommand(CommandObserver co, String name, EntryComponent ec, String comment, String approval, ArrayList<String> parentIds)
	{
		super(co, name, null, ec, parentIds);
		this.setComment(comment);
		this.setApproval(approval);
	}
	
	public String getComment()
	{
		return comment;
	}

	private void setComment(String comment)
	{
		this.comment = comment;
	}

	public String getApproval()
	{
		return approval;
	}

	private void setApproval(String approval)
	{
		this.approval = approval;
	}
	
	public void execute()
	{
		Entries approvalEntries = (Entries)this.getEc();
		approvalEntries.setComment(this.getComment());
		approvalEntries.setStatus(this.getApproval());
		if(this.getApproval().equals("Disapproved"))
		{
			User tutor = User.getUser(approvalEntries.getTutorId());
			ArrayList<String> disapprovedId = new ArrayList<String>();
			disapprovedId.add("Disapproved" + tutor.getId());
			tutor.executeCommand(this.getCo(), "Add", approvalEntries, disapprovedId);
			tutor.persist();
		}
		User approver = User.getUser(approvalEntries.getApproverId());
		approver.executeCommand(this.getCo(), "Remove", approvalEntries, new ArrayList<String>());
		approver.persist();
		setChanged();
		this.notifyObservers(this.getEc());
	}
}
