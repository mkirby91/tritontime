package commands;

import timecard.*;
import java.util.ArrayList;

public class UpdateCommand extends Command
{
	public UpdateCommand(CommandObserver co, String name, Entries entries, EntryComponent ec, ArrayList<String> parentIds)
	{
		super(co, name, entries, ec, parentIds);
	}

	public void execute()
	{
		if(this.getEntries().get(this.getParentIds()).get(this.getEc().getId()) != null)
		{
				this.getEntries().remove(this.getEc(), this.getParentIds());
				this.getEntries().add(this.getEc(), this.getParentIds());
		}
	}
	
}
