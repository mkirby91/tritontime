package commands;

import java.util.Observable;
import java.util.Observer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import timecard.*;
import users.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

public class CommandObserver implements Observer
{
	
	private Key key;
	private String id;
	private HashMap<String,EntryComponent> pendingTimecards;
	private HashMap<String,EntryComponent> approvedTimecards;
	
	public CommandObserver()
	{
		setId("CommandObserver");
		pendingTimecards = new HashMap<String,EntryComponent>();
		approvedTimecards = new HashMap<String,EntryComponent>();
	}
	
	public CommandObserver(HashMap<String,EntryComponent> pendingTimecards, HashMap<String,EntryComponent> approvedTimecards)
	{
		setId("CommandObserver");
		setPendingTimecards(pendingTimecards);
		setApprovedTimecards(approvedTimecards);
	}
	
	public Key getKey()
	{
		return key;
	}
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}
	
	public HashMap<String,EntryComponent> getPendingTimecards()
	{
		return pendingTimecards;
	}
	
	private void setPendingTimecards(HashMap<String,EntryComponent> pendingTimecards)
	{
		this.pendingTimecards = pendingTimecards;
	}
	
	public HashMap<String,EntryComponent> getApprovedTimecards()
	{
		return approvedTimecards;
	}
	
	private void setApprovedTimecards(HashMap<String,EntryComponent> approvedTimecards)
	{
		this.approvedTimecards = approvedTimecards;
	}
	
	public void update(Observable o, Object arg)
	{
		Command c = (Command)o;
		if(c.getName().equals("Submit"))
		{
			Entries submission = (Entries)arg;
			if(submission.getType().equals("Timecard"))
				pendingTimecards.put(submission.getId(), submission);
		}
		else if(c.getName().equals("Approve ClassEntry"))
		{
			Entries classEntry = (Entries)arg;
			String timecardId = classEntry.getParentIds().get(0);
			Entries timecard = (Entries)pendingTimecards.get(timecardId);
			Iterator<EntryComponent> iter = timecard.iterator();
			boolean timecardApproved = true;
			while(iter.hasNext())
			{
				Entries e = (Entries)iter.next();
				if(!e.getStatus().equals("Approved"))
					timecardApproved = false;
			}
			if(timecardApproved)
			{
				pendingTimecards.remove(timecardId);
				User approver = User.getUser(timecard.getApproverId());
				approver.executeCommand(this, "Add", timecard, new ArrayList<String>());
				approver.persist();
			}
		}
		else if(c.getName().equals("Approve Timecard"))
		{
			Entries timecard = (Entries)arg;
			approvedTimecards.put(timecard.getId(), timecard);
			User tutor = User.getUser(timecard.getTutorId());
			ArrayList<String> submittedId = new ArrayList<String>();
			submittedId.add("Submitted" + tutor.getId());
			tutor.executeCommand(this, "Remove", timecard, submittedId);
			ArrayList<String> approvedId = new ArrayList<String>();
			approvedId.add("Approved" + tutor.getId());
			tutor.executeCommand(this, "Add", timecard, approvedId);
			tutor.persist();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static CommandObserver getCommandObserver(String id)
	{
		CommandObserver result = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try
		{
		    Key commandObserverKey = KeyFactory.createKey("CommandObserver", id);
		    Entity commandObserver = datastore.get(commandObserverKey);
		    
		    HashMap<String,EntryComponent> pends = new HashMap<String,EntryComponent>();
		    HashMap<String,EntryComponent> apps = new HashMap<String,EntryComponent>();
		    ArrayList<String> pend = (ArrayList<String>)(commandObserver.getProperty("pendingTimecards"));
		    if (pend==null) { pend = new ArrayList<String>(); }
		    for (String tid:pend) {
		    	EntryComponent ec = Entries.getEntries(tid);
		    	pends.put(tid, ec);
		    }
		    ArrayList<String> app = (ArrayList<String>)(commandObserver.getProperty("approvedTimecards"));
		    if (app==null) { app = new ArrayList<String>(); }
		    for (String tid:app) {
		    	EntryComponent ec = Entries.getEntries(tid);
		    	apps.put(tid, ec);
		    }
		    result = new CommandObserver(pends,
		    							 apps);
		} 
		catch (EntityNotFoundException e)
		{
			return null;
		} 
		finally
		{
			if (txn.isActive())
		        txn.rollback();
		}
		return result;
	}
	
	public void persist()
	{
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try
		{
		    Key commandObserverKey = KeyFactory.createKey("CommandObserver", this.getId());
		    Entity commandObserver = datastore.get(commandObserverKey);
		    commandObserver.setProperty("pendingTimecards", new ArrayList<String>(this.pendingTimecards.keySet()));
		    commandObserver.setProperty("approvedTimecards", new ArrayList<String>(this.approvedTimecards.keySet()));
		    
		    for (EntryComponent ec:this.pendingTimecards.values()) {
				if (ec.getType().equals("DailyHoursEntry")) {
					((Entry)ec).persist();
				} else {
					((Entries)ec).persist();
				}
			}
		    for (EntryComponent ec:this.approvedTimecards.values()) {
				if (ec.getType().equals("DailyHoursEntry")) {
					((Entry)ec).persist();
				} else {
					((Entries)ec).persist();
				}
			}
		    
		    datastore.put(commandObserver);
			System.out.println("Updated CommandObserver " + this.getId());
			txn.commit();
		}
		catch (EntityNotFoundException e)
		{
			Entity commandObserver = new Entity("CommandObserver", this.getId());
			commandObserver.setProperty("pendingTimecards", new ArrayList<String>(this.pendingTimecards.keySet()));
		    commandObserver.setProperty("approvedTimecards", new ArrayList<String>(this.approvedTimecards.keySet()));
		    
		    for (EntryComponent ec:this.pendingTimecards.values()) {
				if (ec.getType().equals("DailyHoursEntry")) {
					((Entry)ec).persist();
				} else {
					((Entries)ec).persist();
				}
			}
		    for (EntryComponent ec:this.approvedTimecards.values()) {
				if (ec.getType().equals("DailyHoursEntry")) {
					((Entry)ec).persist();
				} else {
					((Entries)ec).persist();
				}
			}

		    datastore.put(commandObserver);
		    System.out.println("Added CommandObserver " + this.getId());
		    txn.commit();
		}
		finally
		{
			if(txn.isActive())
				txn.rollback();
		}
	}
}
