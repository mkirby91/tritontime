package commands;

import static org.junit.Assert.*;
import org.junit.Test;
import timecard.*;
import users.*;
import java.util.ArrayList;

public class CommandTest
{
	@Test
	public void testExecuteAddCommand()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		
		Command addCommand = new AddCommand(co, "Add", all, dailyEntry, dailyEntry.getParentIds());
		addCommand.execute();
		assertNotNull(all.get(dailyEntry.getIds()));
	}
	
	@Test
	public void testExecuteAddCommandNull()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		
		Command addCommand = new AddCommand(co, "Add", all, dailyEntry, dailyEntry.getParentIds());
		addCommand.execute();
		ArrayList<String> testIds = new ArrayList<String>();
		testIds.add(timecard.getId());
		testIds.add(classEntry.getId());
		testIds.add("Non existent dailyEntry");
		assertNull(all.get(testIds));
	}
	
	@Test
	public void testExecuteRemoveCommand()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		
		Command removeCommand = new RemoveCommand(co, "Remove", all, dailyEntry, dailyEntry.getParentIds());
		removeCommand.execute();
		assertNotNull(all.get(dailyEntryParentIds));
	}
	
	@Test
	public void testExecuteRemoveCommandNull()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		
		Command removeCommand = new RemoveCommand(co, "Remove", all, dailyEntry, dailyEntry.getParentIds());
		removeCommand.execute();
		assertNull(all.get(dailyEntry.getIds()));
	}
	
	@Test
	public void testExecuteUpdateCommand()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		
		Entry dailyEntryUpdate = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),5,"Grading","All students fail.", dailyEntry.getParentIds());
		Command updateCommand = new UpdateCommand(co, "Update", all, dailyEntryUpdate, dailyEntryUpdate.getParentIds());
		updateCommand.execute();
		Entry e = (Entry)all.get(dailyEntry.getIds());
		assertEquals(dailyEntryUpdate, e);
		assertEquals(5, e.getHours());
		assertEquals("Grading",e.getHoursType());
		assertEquals("All students fail.", e.getHoursDescription());
	}
	
	@Test
	public void testExecuteUpdateCommandNull()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		
		Entry dailyEntryUpdate = new Entry("DailyHoursEntry", "", "1/1/1900",tutor.getId(),5,"Grading","All students fail.", dailyEntry.getParentIds());
		Command updateCommand = new UpdateCommand(co, "Update", all, dailyEntryUpdate, dailyEntryUpdate.getParentIds());
		updateCommand.execute();
		assertNull(all.get(dailyEntryUpdate.getIds()));
	}
	
	@Test
	public void testExecuteSubmitCommand()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Approver professor2  = new Approver("333", "Jim Professor", "Professor", "jprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		Entries classEntry2 = new Entries("ClassEntry", "Class 2", "3/24/1987", tutor, professor2, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds2 = new ArrayList<String>();
		dailyEntryParentIds2.add(timecard.getId());
		dailyEntryParentIds2.add(classEntry2.getId());
		Entry dailyEntry2 = new Entry("DailyHoursEntry", "","3/24/1987",tutor.getId(),4,"Grading","All students fail.", dailyEntryParentIds2);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		all.add(classEntry2, classEntry2.getParentIds());
		all.add(dailyEntry2, dailyEntry2.getParentIds());
		
		Command submitCommand = new SubmitCommand(co, "Submit", timecard, timecard.getParentIds());
		submitCommand.execute();
		assertNotNull(professor.getEntries().get(classEntry.getId()));
		assertNotNull(professor2.getEntries().get(classEntry2.getId()));
		assertNull(tutor.getEntries().get(timecard.getId()));
		assertEquals(classEntry, professor.getEntries().get(classEntry.getId()));
		assertEquals(classEntry2, professor2.getEntries().get(classEntry2.getId()));
	} 
	
	@Test
	public void testExecuteSubmitCommandEmptyTimecard()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Approver professor2  = new Approver("333", "Jim Professor", "Professor", "jprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		all.add(timecard, timecard.getParentIds());
		
		Command submitCommand = new SubmitCommand(co, "Submit", timecard, timecard.getParentIds());
		submitCommand.execute();
		assertEquals(0, professor.getEntries().getEntryComponents().size());
		assertEquals(0, professor2.getEntries().getEntryComponents().size());
		assertEquals(0, hrapprover.getEntries().getEntryComponents().size());
		assertNull(tutor.getEntries().get(timecard.getId()));
	}
	
	@Test
	public void testExecuteApprovalCommandApproved()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		Command submitCommand = new SubmitCommand(co, "Submit", timecard, timecard.getParentIds());
		submitCommand.execute();
		
		Command approvalCommand = new ApprovalCommand(co, "Approve ClassEntry", professor.getEntries().get(classEntry.getId()), "Comment: Approved", "Approved", new ArrayList<String>());
		approvalCommand.execute();
		assertEquals("Approved", classEntry.getStatus());
		assertEquals("Comment: Approved", classEntry.getComment());
		assertNull(professor.getEntries().get(classEntry.getId()));
	}
	
	@Test
	public void testExecuteApprovalCommandDisapproved()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		Approver professor = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Entries all = new Entries("All",tutor);
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",tutor.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		Command submitCommand = new SubmitCommand(co, "Submit", timecard, timecard.getParentIds());
		submitCommand.execute();
		
		Command approvalCommand = new ApprovalCommand(co, "Disapprove ClassEntry", professor.getEntries().get(classEntry.getId()), "Comment: Disapproved", "Disapproved", new ArrayList<String>());
		approvalCommand.execute();
		assertEquals("Disapproved", classEntry.getStatus());
		assertEquals("Comment: Disapproved", classEntry.getComment());
		assertNull(professor.getEntries().get(classEntry.getId()));
		ArrayList<String> disapprovedIds = new ArrayList<String>();
		disapprovedIds.add("Disapproved" + tutor.getId());
		disapprovedIds.add(classEntry.getId());
		assertEquals(classEntry, tutor.getEntries().get(disapprovedIds));
	}

}
