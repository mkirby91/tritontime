package commands;

import timecard.*;
import java.util.ArrayList;

public class AddCommand extends Command
{
	public AddCommand(CommandObserver co, String name, Entries entries, EntryComponent ec, ArrayList<String> parentIds)
	{
		super(co, name, entries, ec, parentIds);
	}
	
	public void execute()
	{
		this.getEntries().add(this.getEc(), this.getParentIds());
	}
}
