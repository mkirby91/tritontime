package commands;

import timecard.*;
import java.util.ArrayList;

public class RemoveCommand extends Command
{
	public RemoveCommand(CommandObserver co, String name, Entries entries, EntryComponent ec, ArrayList<String> parentIds)
	{
		super(co, name, entries, ec, parentIds);
	}
	
	public void execute()
	{
		this.getEntries().remove(this.getEc(), this.getParentIds());
	}
}
