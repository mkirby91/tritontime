package commands;

import java.util.Observable;
import java.util.ArrayList;
import timecard.*;

public abstract class Command extends Observable
{
	private CommandObserver co;
	private String name;
	private Entries entries;
	private EntryComponent ec;
	private ArrayList<String> parentIds;
	
	public Command(CommandObserver co, String name, Entries entries, EntryComponent e, ArrayList<String> parentIds)
	{
		this.setCo(co);
		this.addObserver(co);
		this.setName(name);
		this.setEntries(entries);
		this.setE(e);
		this.setParentIds(parentIds);
	}
	
	public CommandObserver getCo()
	{
		return co;
	}

	private void setCo(CommandObserver co)
	{
		this.co = co;
	}
	
	public String getName()
	{
		return name;
	}

	private void setName(String name)
	{
		this.name = name;
	}

	public Entries getEntries()
	{
		return entries;
	}

	private void setEntries(Entries entries)
	{
		this.entries = entries;
	}

	public EntryComponent getEc()
	{
		return ec;
	}

	private void setE(EntryComponent ec)
	{
		this.ec = ec;
	}
	
	public ArrayList<String> getParentIds()
	{
		return parentIds;
	}

	public void setParentIds(ArrayList<String> parentIds)
	{
		this.parentIds = parentIds;
	}
	
	public abstract void execute();
}
