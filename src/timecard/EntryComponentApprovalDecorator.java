package timecard;

import users.*;
import java.util.ArrayList;

public abstract class EntryComponentApprovalDecorator extends EntryComponent
{
	private String tutorId;
	private String approverId;
	private String status;
	private String comment;
	
	public EntryComponentApprovalDecorator(String type, User user)
	{
		super(type, user == null ? "" : user.getId());
		this.setTutorId(user == null ? "" : user.getId());
		this.setApproverId("");
		this.setComment("");
	}
	
	public EntryComponentApprovalDecorator(String type, String info, String date, String tutorId, String approverId, ArrayList<String> parentIds)
	{
		super(type, info, date, tutorId, parentIds);
		this.setTutorId(tutorId);
		this.setApproverId(approverId);
		this.setStatus("Not Submitted");
		this.setComment("");
	}
	
	public EntryComponentApprovalDecorator(String type, String info, String date, String tutorId, String approverId, ArrayList<String> parentIds, String status, String comment)
	{
		super(type, info, date, tutorId, parentIds);
		this.setTutorId(tutorId);
		this.setApproverId(approverId);
		this.setStatus(status);
		this.setComment(comment);
	}
	
	public String getTutorId()
	{
		return tutorId;
	}
	
	private void setTutorId(String tutorId)
	{
		this.tutorId = tutorId;
	}
	
	public String getApproverId()
	{
		return approverId;
	}
	
	private void setApproverId(String approverId)
	{
		this.approverId = approverId;
	}
	
	public String getStatus()
	{
		return status;
	}
	
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	public String getComment()
	{
		return comment;
	}
	
	public void setComment(String comment)
	{
		this.comment = comment;
	}
}
