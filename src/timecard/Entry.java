package timecard;

import java.util.ArrayList;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

public class Entry extends EntryComponent
{
	private int hours;
	private String hoursType;
	private String hoursDescription;
	
	public Entry(String type, String info, String date, String userId, int hours, String hoursType, String hoursDescription, ArrayList<String> parentIds)
	{
		super(type, info, date, userId, parentIds);
		this.update(hours, hoursType, hoursDescription);
	}
	
	public int getHours()
	{
		return hours;
	}
	
	private void setHours(int hours)
	{
		this.hours = hours;
	}
	
	public String getHoursType()
	{
		return hoursType;
	}
	
	private void setHoursType(String hoursType)
	{
		this.hoursType = hoursType;
	}
	
	public String getHoursDescription()
	{
		return hoursDescription;
	}
	
	private void setHoursDescription(String hoursDescription)
	{
		this.hoursDescription = hoursDescription;
	}
	
	public void update(int hours, String hoursType, String hoursDescription)
	{
		this.setHours(hours);
		this.setHoursType(hoursType);
		this.setHoursDescription(hoursDescription);
	}
	
	@SuppressWarnings("unchecked")
	public static Entry getEntry(String id)
	{
		Entry result = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try
		{
		    Key entryKey = KeyFactory.createKey("Entry", id);
		    com.google.appengine.api.datastore.Entity entry = datastore.get(entryKey);
		    result = new Entry((String)entry.getProperty("type"), 
		    				   (String)entry.getProperty("info"),
		    				   (String)entry.getProperty("date"),
		    				   (String)entry.getProperty("userId"),
		    				   ((Long)entry.getProperty("hours")).intValue(),
		    				   (String)entry.getProperty("hoursType"),
		    				   (String)entry.getProperty("hoursDescription"),
		    				   (ArrayList<String>)entry.getProperty("parentIds"));
		} 
		catch (EntityNotFoundException e)
		{
			return null;
		} 
		finally
		{
			if(txn.isActive())
				txn.rollback();
		}
		return result;
	}
	
	public void persist()
	{
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try
		{
		    Key entryKey = KeyFactory.createKey("Entry", this.getId());
		    Entity entry = datastore.get(entryKey);
		    entry.setProperty("ID", this.getId());
		    entry.setProperty("type", this.getType());
		    entry.setProperty("info", this.getInfo());
		    entry.setProperty("date", this.getDate());
		    entry.setProperty("userId", this.getUserId());
		    entry.setProperty("parentIds", this.getParentIds());
		    entry.setProperty("hours", this.getHours());
		    entry.setProperty("hoursType", this.getHoursType());
		    entry.setProperty("hoursDescription", this.getHoursDescription());
			System.out.println("Updated entries " + this.getId());
			txn.commit();
		}
		catch (EntityNotFoundException e)
		{
			Entity entry = new Entity("Entry", this.getId());
			entry.setProperty("ID", this.getId());
		    entry.setProperty("type", this.getType());
		    entry.setProperty("info", this.getInfo());
		    entry.setProperty("date", this.getDate());
		    entry.setProperty("parentIds", this.getParentIds());
		    entry.setProperty("hours", this.getHours());
		    entry.setProperty("hoursType", this.getHoursType());
		    entry.setProperty("hoursDescription", this.getHoursDescription());

		    datastore.put(entry);
		    System.out.println("Added entries " + this.getId());
		    txn.commit();
		}
		finally
		{
			if(txn.isActive())
				txn.rollback();
		}
	}
}
