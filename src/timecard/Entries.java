package timecard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import users.*;

public class Entries extends EntryComponentApprovalDecorator
{
	private HashMap<String,EntryComponent> entryComponents;
	
	public Entries(String type, User user)
	{
		super(type, user);
		entryComponents = new HashMap<String,EntryComponent>();
	}
	
	public Entries(String type, String info, String date, String tutorId, String approverId, ArrayList<String> parentIds)
	{
		super(type, info, date, tutorId, approverId, parentIds);
		entryComponents = new HashMap<String,EntryComponent>();
	}
	
	public Entries(String type, String info, String date, String tutorId, String approverId, ArrayList<String> parentIds, HashMap<String,EntryComponent> entryComponents)
	{
		super(type, info, date, tutorId, approverId, parentIds);
		setEntryComponents(entryComponents);
	}
	
	public Entries(String type, String info, String date, String tutorId, String approverId, ArrayList<String> parentIds, HashMap<String,EntryComponent> entryComponents, String status, String comment)
	{
		super(type, info, date, tutorId, approverId, parentIds, status, comment);
		setEntryComponents(entryComponents);
	}
	
	public HashMap<String,EntryComponent> getEntryComponents()
	{
		return entryComponents;
	}
	
	private void setEntryComponents(HashMap<String,EntryComponent> entryComponents)
	{
		this.entryComponents = entryComponents;
	}
	
	public Iterator<EntryComponent> iterator()
	{
		return entryComponents.values().iterator();
	}
	
	public int getHours()
	{
		int totalHours = 0;
		Iterator<EntryComponent> iter = this.iterator();
		while(iter.hasNext())
		{
			EntryComponent e = iter.next();
			totalHours += e.getHours();
		}
		return totalHours;
	}
	
	public void add(EntryComponent e, ArrayList<String> parentIds)
	{
		Entries parent = (Entries)get(parentIds);
		if(parent != null)
		{
			if(parent.get(e.getId()) == null) // && !"Submitted".equals(parent.getStatus()) && !"Approved".equals(parent.getStatus()))
				parent.getEntryComponents().put(e.getId(),e);
		}
	}
	
	public void remove(EntryComponent e, ArrayList<String> parentIds)
	{
		Entries parent = (Entries)get(parentIds);
		if(parent != null)
		{
			// if(!"Submitted".equals(parent.getStatus()) && !"Approved".equals(parent.getStatus()))
				parent.getEntryComponents().remove(e.getId());
		}
	}
	
	public EntryComponent get(String id)
	{
		return entryComponents.get(id);
	}
	
	public EntryComponent get(ArrayList<String> ids)
	{
		EntryComponent e = this;
		for(String id : ids)
		{
			if(e == null)
				return null;
			else
				e = e.get(id);
		}
		return e;
	}
	
	@SuppressWarnings("unchecked")
	public static Entries getEntries(String id)
	{
		Entries result = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try
		{
			HashMap<String,EntryComponent> ents = new HashMap<String,EntryComponent>();
		    Key entriesKey = KeyFactory.createKey("Entries", id);
		    Entity entries = datastore.get(entriesKey);
		    
		    ArrayList<String> myent = (ArrayList<String>)(entries.getProperty("entryComponents"));
		    if (myent==null) { myent = new ArrayList<String>(); }
		    for (String eid:myent) {
		    		
		    		EntryComponent ec = Entry.getEntry(eid);
		    		if (ec == null) {
		    			ec = Entries.getEntries(eid);
		    		}
		    		ents.put(eid, ec);
		    }
		    result = new Entries((String)entries.getProperty("type"), 
		    					 (String)entries.getProperty("info"),
		    					 (String)entries.getProperty("date"),
		    					 (String)entries.getProperty("tutor"),
		    					 (String)entries.getProperty("approver"),
		    					 (ArrayList<String>)entries.getProperty("parentIds"),
		    					 ents,
		    					 (String)entries.getProperty("status"),
		    					 (String)entries.getProperty("comment"));
		} 
		catch (EntityNotFoundException e)
		{
			return null;
		} 
		finally
		{
			if (txn.isActive())
		        txn.rollback();
		}
		return result;
	}
	
	public void persist()
	{
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try
		{
		    Key entriesKey = KeyFactory.createKey("Entries", this.getId());
		    Entity entries = datastore.get(entriesKey);
		    entries.setProperty("ID", this.getId());
		    entries.setProperty("type", this.getType());
		    entries.setProperty("info", this.getInfo());
		    entries.setProperty("date", this.getDate());
		    entries.setProperty("parentIds", this.getParentIds());
		    entries.setProperty("tutor", this.getTutorId());
		    entries.setProperty("approver", this.getApproverId());
		    entries.setProperty("entryComponents", new ArrayList<String>(this.getEntryComponents().keySet()));
		    entries.setProperty("status", this.getStatus());
		    entries.setProperty("comment", this.getComment());
			System.out.println("Updated entries " + this.getId());
			for (EntryComponent e:this.getEntryComponents().values()) {
				if (e.getType().equals("DailyHoursEntry")) {
					((Entry)e).persist();
				} else {
					((Entries)e).persist();
				}
			}
			datastore.put(entries);
			txn.commit();
		}
		catch (EntityNotFoundException e)
		{
			Entity entries = new Entity("Entries", this.getId());
			entries.setProperty("ID", this.getId());
		    entries.setProperty("type", this.getType());
		    entries.setProperty("info", this.getInfo());
		    entries.setProperty("date", this.getDate());
		    entries.setProperty("parentIds", this.getParentIds());
		    entries.setProperty("tutor", this.getTutorId());
		    entries.setProperty("approver", this.getApproverId());
		    entries.setProperty("entryComponents", new ArrayList<String>(this.getEntryComponents().keySet()));
		    entries.setProperty("status", this.getStatus());
		    entries.setProperty("comment", this.getComment());
		    
		    for (EntryComponent ec:this.getEntryComponents().values()) {
				if (ec.getType().equals("DailyHoursEntry")) {
					((Entry)ec).persist();
				} else {
					((Entries)ec).persist();
				}
			}
		    
		    datastore.put(entries);
		    System.out.println("Added entries " + this.getId());
		    txn.commit();
		}
		finally
		{
			if(txn.isActive())
				txn.rollback();
		}
	}
}
