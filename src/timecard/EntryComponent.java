package timecard;

import java.util.ArrayList;
import com.google.appengine.api.datastore.Key;

public abstract class EntryComponent
{
    private Key key;
	private String id;
	private String type;
	private String info;
	private String date;
	private String userId;
	private ArrayList<String> parentIds;
	
	public EntryComponent(String type, String userId)
	{
		this.setId("", type, "", userId);
		this.setType(type);
		this.setInfo("");
		this.setDate("");
		this.setUserId(userId);
		parentIds = new ArrayList<String>();
	}
	
	public EntryComponent(String type, String info, String date, String userId, ArrayList<String> parentIds)
	{
		this.setId(type, info, date, userId);
		this.setInfo(info);
		this.setType(type);
		this.setDate(date);
		this.setParentIds(parentIds);
	}
	
	public Key getKey()
	{
		return key;
	}
	
	public String getId()
	{
		return id;
	}
	
	private void setId(String type, String info, String date, String username)
	{
		this.id = type + info + date + username;
	}
	
	public String getType()
	{
		return type;
	}

	private void setType(String type)
	{
		this.type = type;
	}
	
	public String getInfo()
	{
		return info;
	}
	
	private void setInfo(String info)
	{
		this.info = info;
	}
	
	public String getDate()
	{
		return date;
	}
	
	private void setDate(String date)
	{
		this.date = date;
	}
	
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	
	public ArrayList<String> getParentIds()
	{
		return parentIds;
	}
	
	private void setParentIds(ArrayList<String> parentIds)
	{
		this.parentIds = parentIds;
	}
	
	public ArrayList<String> getIds()
	{
		ArrayList<String> result = new ArrayList<String>();
		for(String parentId : this.getParentIds())
			result.add(parentId);
		result.add(this.getId());
		return result;
	}
	
	public abstract int getHours();
	
	public void add(EntryComponent e, ArrayList<String> parentIds)
	{
		throw new UnsupportedOperationException();
	}
	
	public void remove(EntryComponent e, ArrayList<String> parentIds)
	{
		throw new UnsupportedOperationException();
	}
	
	public EntryComponent get(String id)
	{
		throw new UnsupportedOperationException();
	}
	
	public EntryComponent get(ArrayList<String> ids)
	{
		throw new UnsupportedOperationException();
	}
}
