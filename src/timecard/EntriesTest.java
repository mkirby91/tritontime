package timecard;

import static org.junit.Assert.*;
import org.junit.Test;
import users.*;
import java.util.ArrayList;

public class EntriesTest
{	
	@Test
	public void testGetString()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> parentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, parentIds);
		
		all.getEntryComponents().put(timecard.getId(), timecard);
		assertNotNull(all.get(timecard.getId()));
	}
	
	@Test
	public void testGetStringNull()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> parentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, parentIds);
		
		all.getEntryComponents().put(timecard.getId(), timecard);
		assertNull(all.get("Nonexistent Timecard"));
	}
	
	@Test
	public void testGetStringArray()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(), 2,"Discussion","No Description", dailyEntryParentIds);
		all.getEntryComponents().put(timecard.getId(), timecard);
		timecard.getEntryComponents().put(classEntry.getId(), classEntry);
		classEntry.getEntryComponents().put(dailyEntry.getId(), dailyEntry);
		
		assertNotNull(all.get(dailyEntryParentIds));
	}
	
	@Test
	public void testGetStringArrayNull()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.getEntryComponents().put(timecard.getId(), timecard);
		timecard.getEntryComponents().put(classEntry.getId(), classEntry);
		classEntry.getEntryComponents().put(dailyEntry.getId(), dailyEntry);
		
		ArrayList<String> testList = new ArrayList<String>();
		testList.add(timecard.getId());
		testList.add(classEntry.getId());
		testList.add("Nonexistent DailyEntry");
		assertNull(all.get(testList));
	}

	@Test
	public void testAdd()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.getEntryComponents().put(timecard.getId(), timecard);
		timecard.getEntryComponents().put(classEntry.getId(), classEntry);
		
		all.add(dailyEntry, dailyEntry.getParentIds());
		assertNotNull(all.get(dailyEntryParentIds));
	}
	
	@Test
	public void testAddNull()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.getEntryComponents().put(timecard.getId(), timecard);
		timecard.getEntryComponents().put(classEntry.getId(), classEntry);
		
		all.add(dailyEntry, dailyEntry.getParentIds());
		ArrayList<String> testList = new ArrayList<String>();
		testList.add(timecard.getId());
		testList.add(classEntry.getId());
		testList.add("Nonexistent DailyEntry");
		assertNull(all.get(testList));
		assertNull(all.get(testList));
	}
	
	@Test
	public void testAddSubmitted()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.getEntryComponents().put(timecard.getId(), timecard);
		timecard.getEntryComponents().put(classEntry.getId(), classEntry);
		classEntry.setStatus("Submitted");
		
		all.add(dailyEntry, dailyEntry.getParentIds());
		assertNull(all.get(dailyEntry.getIds()));
	}
	
	@Test
	public void testAddApproved()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.getEntryComponents().put(timecard.getId(), timecard);
		timecard.getEntryComponents().put(classEntry.getId(), classEntry);
		classEntry.setStatus("Approved");
		
		all.add(dailyEntry, dailyEntry.getParentIds());
		assertNull(all.get(dailyEntry.getIds()));
	}

	@Test
	public void testRemove()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All", user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		
		all.remove(dailyEntry, dailyEntry.getParentIds());
		assertNull(all.get(dailyEntry.getIds()));
		all.remove(classEntry, classEntry.getParentIds());
		assertNull(all.get(classEntry.getIds()));
	}
	
	@Test
	public void testRemoveNull()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All",user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		
		all.remove(new Entry("Test","Test","Test","Test",2,"Test","Test", classEntryParentIds), classEntryParentIds);
		assertNotNull(all.get(timecard.getId()));
		assertNotNull(all.get(classEntryParentIds));
		assertNotNull(all.get(dailyEntryParentIds));
	}
	
	@Test
	public void testRemoveSubmitted()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All",user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		classEntry.setStatus("Submitted");
		
		all.remove(dailyEntry, dailyEntry.getParentIds());
		assertNotNull(all.get(dailyEntryParentIds));
	}
	
	@Test
	public void testRemoveApproved()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All",user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry = new Entry("DailyHoursEntry", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry, dailyEntry.getParentIds());
		classEntry.setStatus("Approved");
		
		all.remove(dailyEntry, dailyEntry.getParentIds());
		assertNotNull(all.get(dailyEntryParentIds));
	}
	
	@Test
	public void testGetHours()
	{
		Tutor user = new Tutor("12345", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver approver = new Approver("12345", "Bob Approver", "Approver", "bapprover@ucsd.edu", "CSE");
		Entries all = new Entries("All",user);
		ArrayList<String> timecardParentIds = new ArrayList<String>();
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", user, approver, timecardParentIds);
		ArrayList<String> classEntryParentIds = new ArrayList<String>();
		classEntryParentIds.add(timecard.getId());
		Entries classEntry = new Entries("ClassEntry", "Class 1", "3/24/1987", user, approver, classEntryParentIds);
		ArrayList<String> dailyEntryParentIds = new ArrayList<String>();
		dailyEntryParentIds.add(timecard.getId());
		dailyEntryParentIds.add(classEntry.getId());
		Entry dailyEntry1 = new Entry("DailyHoursEntry1", "", "3/24/1987",user.getId(),2,"Discussion","No Description", dailyEntryParentIds);
		Entry dailyEntry2 = new Entry("DailyHoursEntry2", "2", "3/24/1987",user.getId(),5,"Grading","Description", dailyEntryParentIds);
		all.add(timecard, timecard.getParentIds());
		all.add(classEntry, classEntry.getParentIds());
		all.add(dailyEntry1, dailyEntry1.getParentIds());
		all.add(dailyEntry2, dailyEntry2.getParentIds());
		
		assertEquals(7, timecard.getHours());
	}
}
