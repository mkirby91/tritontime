package timecard;

import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.Test;

public class EntryTest
{
	@Test
	public void testUpdate()
	{
		ArrayList<String> parentIds = new ArrayList<String>();
		parentIds.add("Timecard");
		parentIds.add("ClassEntry");
		Entry e = new Entry("DailyHoursEntry", "Info", "3/24/1987","UserId",1,"Discussion","No Description", parentIds);
		e.update(3, "Grading", "No one passed the midterm.");
		assertEquals(e.getHours(),3);
		assertEquals(e.getHoursType(),"Grading");
		assertEquals(e.getHoursDescription(),"No one passed the midterm.");
	}
}
