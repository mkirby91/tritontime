package users;

import com.google.appengine.api.datastore.Key;

public class UserClass
{
    private Key key;
	private String userId;
	private String className;
	
	public UserClass(String userId, String className)
	{
		setUserId(userId);
		setClassName(className);
	}
	
	public Key getKey()
	{
		return key;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getClassName()
	{
		return className;
	}

	public void setClassName(String className)
	{
		this.className = className;
	}
}
