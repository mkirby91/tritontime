package users;

import commands.*;
import timecard.*;
import java.util.ArrayList;


import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

public abstract class User
{
    private Key key;
	private String id;
	private String name;
	private String role;
	private String email;
	private String department;
	private Entries entries;
	
	public User(String id, String name, String role, String email, String department)
	{
		this.setId(id);
		this.setName(name);
		this.setRole(role);
		this.setEmail(email);
		this.setDepartment(department);
		entries = new Entries("All", this);
	}
	
	public User(String id, String name, String role, String email, String department, Entries entries)
	{
		this.setId(id);
		this.setName(name);
		this.setRole(role);
		this.setEmail(email);
		this.setDepartment(department);
		this.entries = entries;
	}
	
	public Key getKey()
	{
		return key;
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getRole()
	{
		return role;
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	public String getDepartment()
	{
		return department;
	}
	
	public void setDepartment(String department)
	{
		this.department = department;
	}
	
	public Entries getEntries()
	{
		return entries;
	}
	
	public void executeCommand(CommandObserver co, String commandName, EntryComponent e, ArrayList<String> parentIds)
	{
		if(commandName.equals("Add"))
			new AddCommand(co, commandName, this.getEntries(), e, parentIds).execute();
		else if(commandName.equals("Remove"))
			new RemoveCommand(co, commandName, this.getEntries(), e, parentIds).execute();
	}
	
	public static User getUser(String id) {
		System.out.println("testing"+id);
		if (id == null) { System.out.println("fuck"+id); return null; }
		User result = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try {
		    Key userKey = KeyFactory.createKey("User", id);
		    Entity user = datastore.get(userKey);
		    System.out.println("yay"+id);
		    if (((String)user.getProperty("role")).equals("Tutor")) {
		    	result = new Tutor(id, (String)user.getProperty("name"), "Tutor", (String)user.getProperty("email"), (String)user.getProperty("department"),Entries.getEntries((String)user.getProperty("entries")));
		    } else if (((String)user.getProperty("role")).equals("Professor")) {
		    	result = new Approver(id, (String)user.getProperty("name"), "Professor", (String)user.getProperty("email"), (String)user.getProperty("department"),Entries.getEntries((String)user.getProperty("entries")));
		    } else {
		    	result = new Approver(id, (String)user.getProperty("name"), "HRApprover", (String)user.getProperty("email"), (String)user.getProperty("department"),Entries.getEntries((String)user.getProperty("entries")));
		    }
		} catch (EntityNotFoundException e) {
			System.out.println("nay"+id);
			return null;
		} finally {
			if (txn.isActive()) {
		        txn.rollback();
		    }
		}
		return result;
	}
	public void persist() {
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try {
		    Key userKey = KeyFactory.createKey("User", id);
		    Entity user = datastore.get(userKey);
		    user.setProperty("name",name);
			user.setProperty("role", role);
			user.setProperty("email", email);
			user.setProperty("department", department);
			user.setProperty("entries", entries.getId());
			datastore.put(user);
			System.out.println("Updated user "+name);
			txn.commit();
			entries.persist();
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", id);
			user.setProperty("name",name);
			user.setProperty("role", role);
			user.setProperty("email", email);
			user.setProperty("department", department);
			user.setProperty("entries", entries.getId());
		    datastore.put(user);
		    System.out.println("Added user "+name);
		    txn.commit();
		    entries.persist();
		} finally {
			if (txn.isActive()) {
		        txn.rollback();
		    }
		}
	}
}
