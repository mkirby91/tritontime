package users;

import com.google.appengine.api.datastore.Key;

public class UserDepartment
{
    private Key key;
	private String userId;
	private String department;
	
	public UserDepartment(String userId, String department)
	{
		setUserId(userId);
		setDepartment(department);
	}
	
	public Key getKey()
	{
		return key;
	}

	public String getUserId()
	{
		return userId;
	}

	private void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getDepartment()
	{
		return department;
	}

	private void setDepartment(String department)
	{
		this.department = department;
	}

}
