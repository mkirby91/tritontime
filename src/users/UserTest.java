package users;

import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.Test;
import commands.*;
import timecard.*;

public class UserTest
{

	@Test
	public void fullProcessTest()
	{
		CommandObserver co = new CommandObserver();
		Tutor tutor = new Tutor("111", "Joe Tutor", "Tutor", "jtutor@ucsd.edu", "CSE");
		Approver professor1 = new Approver("222", "Bob Professor", "Professor", "bprofessor@ucsd.edu", "CSE");
		Approver professor2 = new Approver("333", "Jim Professor", "Professor", "jprofessor@ucsd.edu", "CSE");
		Approver hrapprover = new Approver("444", "Sally HR", "HR Approver", "shrapprover@ucsd.edu", "CSE");
		
		Entries timecard = new Entries("Timecard", "CSE", "3/24/1987", tutor, hrapprover, new ArrayList<String>());
		Entries classEntry1 = new Entries("ClassEntry", "Class 1", "3/24/1987", tutor, professor1, timecard.getIds());
		Entries classEntry2 = new Entries("ClassEntry", "Class 2", "3/24/1987", tutor, professor2, timecard.getIds());
		Entry dailyEntry1 = new Entry("DailyHoursEntry", "", "3/24/1987", tutor.getId(), 2, "Discussion", "Description", classEntry1.getIds());
		Entry dailyEntry2 = new Entry("DailyHoursEntry2", "", "3/24/1987", tutor.getId(), 2, "Discussion", "Description", classEntry2.getIds());
		
		tutor.executeCommand(co, "Add", timecard, timecard.getParentIds());
		tutor.executeCommand(co, "Add", classEntry1, classEntry1.getParentIds());
		tutor.executeCommand(co, "Add", classEntry2, classEntry2.getParentIds());
		tutor.executeCommand(co, "Add", dailyEntry1, dailyEntry1.getParentIds());
		tutor.executeCommand(co, "Add", dailyEntry2, dailyEntry2.getParentIds());
		
		assertEquals(timecard, tutor.getEntries().get(timecard.getIds()));
		assertEquals(classEntry1, tutor.getEntries().get(classEntry1.getIds()));
		assertEquals(classEntry2, tutor.getEntries().get(classEntry2.getIds()));
		assertEquals(dailyEntry1, tutor.getEntries().get(dailyEntry1.getIds()));
		assertEquals(dailyEntry2, tutor.getEntries().get(dailyEntry2.getIds()));
		
		Entry dailyEntryUpdate = new Entry("DailyHoursEntry2", "", "3/24/1987", tutor.getId(), 3, "Grading", "New Description", classEntry2.getIds());
		tutor.executeCommand(co, "Update", dailyEntryUpdate, classEntry2.getIds());
		
		assertEquals(dailyEntryUpdate, tutor.getEntries().get(dailyEntryUpdate.getIds()));
		
		tutor.executeCommand(co, "Submit", timecard, timecard.getParentIds());
		
		assertEquals(classEntry1, professor1.getEntries().get(classEntry1.getId()));
		assertEquals(classEntry2, professor2.getEntries().get(classEntry2.getId()));
		assertEquals(timecard, co.getPendingTimecards().get(timecard.getId()));
		ArrayList<String> submittedIds1 = new ArrayList<String>();
		submittedIds1.add("Submitted" + tutor.getId());
		submittedIds1.add(timecard.getId());
		assertEquals(timecard, tutor.getEntries().get(submittedIds1));
		
		professor1.executeCommand(co, "Approve ClassEntry", "Comment: Approved", classEntry1, classEntry1.getIds());
		professor2.executeCommand(co, "Disapprove ClassEntry", "Comment: Disapproved", classEntry2, classEntry2.getIds());
		
		assertNull(professor1.getEntries().get(classEntry1.getId()));
		assertNull(professor2.getEntries().get(classEntry2.getId()));
		assertEquals("Approved", classEntry1.getStatus());
		assertEquals("Disapproved", classEntry2.getStatus());
		ArrayList<String> disapprovedIds1 = new ArrayList<String>();
		disapprovedIds1.add("Disapproved" + tutor.getId());
		disapprovedIds1.add(classEntry2.getId());
		assertEquals(classEntry2, tutor.getEntries().get(disapprovedIds1));
		
		ArrayList<String> disapprovedIds2 = new ArrayList<String>();
		disapprovedIds2.add("Disapproved" + tutor.getId());
		disapprovedIds2.add(timecard.getId());
		disapprovedIds2.add(classEntry2.getId());
		Entry dailyEntryUpdate2 = new Entry("DailyHoursEntry2", "", "3/24/1987", tutor.getId(), 2, "Tutoring", "New New Description", disapprovedIds2);
		tutor.executeCommand(co, "Update", dailyEntryUpdate2, disapprovedIds1);
		
		ArrayList<String> disapprovedIds3 = new ArrayList<String>();
		disapprovedIds3.add("Disapproved" + tutor.getId());
		disapprovedIds3.add(classEntry2.getId());
		disapprovedIds3.add(dailyEntryUpdate2.getId());
		assertEquals(dailyEntryUpdate2, tutor.getEntries().get(disapprovedIds3));
		
		
		tutor.executeCommand(co, "Submit", classEntry2, disapprovedIds1);
		
		assertEquals(classEntry2, professor2.getEntries().get(classEntry2.getId()));
		assertNull(tutor.getEntries().get(disapprovedIds1));
		assertNull(co.getPendingTimecards().get(classEntry2.getId()));
				
		professor2.executeCommand(co, "Approve ClassEntry", "Comment: Approved", classEntry2, classEntry2.getParentIds());
		
		assertEquals(timecard, hrapprover.getEntries().get(timecard.getId()));
		assertNull(professor2.getEntries().get(classEntry2.getId()));
		
		hrapprover.executeCommand(co, "Approve Timecard", "Comment: Approved", timecard, timecard.getParentIds());
		
		assertNull(hrapprover.getEntries().get(timecard.getId()));
		ArrayList<String> approvedIds = new ArrayList<String>();
		approvedIds.add("Approved" + tutor.getId());
		approvedIds.add(timecard.getId());
		assertEquals(timecard, co.getApprovedTimecards().get(timecard.getId()));
		assertNull(co.getPendingTimecards().get(timecard.getId()));
		assertNull(tutor.getEntries().get(submittedIds1));
		assertEquals(timecard, tutor.getEntries().get(approvedIds));
	}

}
