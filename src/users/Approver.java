package users;

import timecard.*;
import commands.*;
import java.util.ArrayList;

public class Approver extends User
{
	public Approver(String id, String name, String role, String email, String department)
	{
		super(id,name,role,email,department);
	}
	
	public Approver(String id, String name, String role, String email, String department,Entries entries)
	{
		super(id,name,role,email,department,entries);
	}
	
	public void executeCommand(CommandObserver co, String commandName, String comment, EntryComponent e, ArrayList<String> parentIds)
	{
		if(commandName.equals("Approve ClassEntry"))
			new ApprovalCommand(co, commandName, e, comment, "Approved", parentIds).execute();
		else if(commandName.equals("Disapprove ClassEntry"))
			new ApprovalCommand(co, commandName, e, comment, "Disapproved", parentIds).execute();
		else if(commandName.equals("Approve Timecard"))
			new ApprovalCommand(co, commandName, e, comment, "Approved", parentIds).execute();
		else if(commandName.equals("Disapprove Timecard"))
			new ApprovalCommand(co, commandName, e, comment, "Disapproved", parentIds).execute();
	}
}
