package users;

import commands.*;
import timecard.*;
import java.util.ArrayList;

public class Tutor extends User
{
	public Tutor(String id, String name, String role, String email, String department)
	{
		super(id,name,role,email,department);
		Entries submitted = new Entries("Submitted", this);
		submitted.setStatus("Submitted");
		submitted.setComment("");
		this.getEntries().add(submitted, new ArrayList<String>());
		Entries disapproved = new Entries("Disapproved", this);
		disapproved.setStatus("Disapproved");
		disapproved.setComment("");
		this.getEntries().add(disapproved, new ArrayList<String>());
		Entries approved = new Entries("Approved", this);
		approved.setStatus("Approved");
		approved.setComment("");
		this.getEntries().add(approved, new ArrayList<String>());
	}
	
	public Tutor(String id, String name, String role, String email, String department, Entries entries)
	{
		super(id,name,role,email,department,entries);
	}
	
	public void executeCommand(CommandObserver co, String commandName, EntryComponent e, ArrayList<String> parentIds)
	{
		if(commandName.equals("Update"))
			new UpdateCommand(co, commandName, this.getEntries(), e, parentIds).execute();
		else if (commandName.equals("Submit"))
			new SubmitCommand(co, commandName, e, parentIds).execute();
		else
			super.executeCommand(co, commandName, e, parentIds);
	}
}
