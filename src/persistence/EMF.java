package persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class EMF {
    private static final EntityManagerFactory emfInstance = Persistence.createEntityManagerFactory("transactions-optional");
    private static EntityManager emInstance;
    
    private EMF() {}

    public static EntityManagerFactory get() {
        return emfInstance;
    }
    
    public static EntityManager getEntityManager(){
    	if(emInstance == null || !emInstance.isOpen()){
    		emInstance = get().createEntityManager();
    	}
    	return emInstance;
    }
}