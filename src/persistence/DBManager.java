package persistence;

import users.*;
import commands.*;
import timecard.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import java.util.ArrayList;

public class DBManager
{

	public static void populateDB()
	{
		CommandObserver co = new CommandObserver();
		
		Tutor tutor1 = new Tutor("T111", "Agnes Apple", "Tutor", "agnesapple@ucsd.edu", "CSE");
		Tutor tutor2 = new Tutor("T222", "Billy Bubblegum", "Tutor", "billybubblegum@ucsd.edu", "CSE");
		Tutor tutor3 = new Tutor("T333", "Cecelia Candycorn", "Tutor", "ceceliacandycorn@ucsd.edu", "CSE");
		Approver professor1 = new Approver("P111", "Rick Ord", "Professor", "rickord@ucsd.edu", "CSE");
		Approver professor2 = new Approver("P222", "Victor Vianu", "Professor", "victorvianu@ucsd.edu", "CSE");
		Approver professor3 = new Approver("P333", "Michael Taylor", "Professor", "michaeltaylor@ucsd.edu", "CSE");
		Approver hr1 = new Approver("A111", "Peggy Penguin", "HRApprover", "peggypenguin@ucsd.edu", "CSE");
		Approver hr2 = new Approver("A222", "Zoe Zhang", "HRApprover", "zoezhang@ucsd.edu", "CSE");
		Approver hr3 = new Approver("A333", "Human Resources", "HRApprover", "hresources@ucsd.edu", "CSE");
		
		co.persist();
		tutor1.persist();
		tutor2.persist();
		tutor3.persist();
		professor1.persist();
		professor2.persist();
		professor3.persist();
		hr1.persist();
		hr2.persist();
		hr3.persist();
		
		tutor1 = (Tutor)User.getUser("T111");
		tutor2 = (Tutor)User.getUser("T222");
		tutor3 = (Tutor)User.getUser("T333");
		professor1 = (Approver)User.getUser("P111");
		professor2 = (Approver)User.getUser("P222");
		professor3 = (Approver)User.getUser("P333");
		hr1 = (Approver)User.getUser("A111");
		hr2 = (Approver)User.getUser("A222");
		hr3 = (Approver)User.getUser("A333");
		
		Entries timecard1 = new Entries("Timecard", " ", "6/8/13", tutor1.getId(), hr1.getId(), new ArrayList<String>());
		Entries timecard2 = new Entries("Timecard", " ", "6/15/13", tutor1.getId(), hr1.getId(), new ArrayList<String>());
		ArrayList<String> classEntry1ParentIds = new ArrayList<String>();
		classEntry1ParentIds.add(timecard1.getId());
		Entries classEntry11 = new Entries("ClassEntry", "Compilers", "6/8/13", tutor1.getId(), professor1.getId(), classEntry1ParentIds);
		Entries classEntry12 = new Entries("ClassEntry", "Databases", "6/8/13", tutor1.getId(), professor2.getId(), classEntry1ParentIds);
		ArrayList<String> dailyEntry1ParentIds = new ArrayList<String>();
		dailyEntry1ParentIds.add(timecard1.getId());
		dailyEntry1ParentIds.add(classEntry11.getId());
		Entry dailyEntry111 = new Entry("DailyHoursEntry", " ", "6/5/13", tutor1.getId(), 4, "Discussion", "ER Review", dailyEntry1ParentIds);
		Entry dailyEntry112 = new Entry("DailyHoursEntry", " ", "6/6/13", tutor1.getId(), 2, "Grading", "Midterm 1 grading", dailyEntry1ParentIds);
		ArrayList<String> dailyEntry2ParentIds = new ArrayList<String>();
		dailyEntry2ParentIds.add(timecard1.getId());
		dailyEntry2ParentIds.add(classEntry12.getId());
		Entry dailyEntry121 = new Entry("DailyHoursEntry", " ", "6/3/13", tutor1.getId(), 1, "Discussion", "MIPS review", dailyEntry2ParentIds);
		Entry dailyEntry122 = new Entry("DailyHoursEntry", " ", "6/4/13", tutor1.getId(), 3, "Grading", "Project Grading", dailyEntry2ParentIds);
		
		tutor1.executeCommand(co, "Add", timecard1, timecard1.getParentIds());
		tutor1.executeCommand(co, "Add", timecard2, timecard2.getParentIds());
		tutor1.executeCommand(co, "Add", classEntry11, classEntry11.getParentIds());
		tutor1.executeCommand(co, "Add", classEntry12, classEntry12.getParentIds());
		tutor1.executeCommand(co, "Add", dailyEntry111, dailyEntry111.getParentIds());
		tutor1.executeCommand(co, "Add", dailyEntry112, dailyEntry112.getParentIds());
		tutor1.executeCommand(co, "Add", dailyEntry121, dailyEntry121.getParentIds());
		tutor1.executeCommand(co, "Add", dailyEntry122, dailyEntry122.getParentIds());
		
		Entries timecard3 = new Entries("Timecard", " ", "6/8/13", tutor2.getId(), hr2.getId(), new ArrayList<String>());
		Entries timecard4 = new Entries("Timecard", " ", "6/15/13", tutor2.getId(), hr2.getId(), new ArrayList<String>());
		ArrayList<String> classEntry3ParentIds = new ArrayList<String>();
		classEntry3ParentIds.add(timecard3.getId());
		Entries classEntry31 = new Entries("ClassEntry", "Databases", "6/8/13", tutor2.getId(), professor2.getId(), classEntry3ParentIds);
		Entries classEntry32 = new Entries("ClassEntry", "Comp Arch", "6/8/13", tutor2.getId(), professor3.getId(), classEntry3ParentIds);
		ArrayList<String> dailyEntry3ParentIds = new ArrayList<String>();
		dailyEntry3ParentIds.add(timecard3.getId());
		dailyEntry3ParentIds.add(classEntry31.getId());
		Entry dailyEntry311 = new Entry("DailyHoursEntry", " ", "6/5/13", tutor2.getId(), 6, "Grading", "Midterm 1 Grading", dailyEntry3ParentIds);
		Entry dailyEntry312 = new Entry("DailyHoursEntry", " ", "6/6/13", tutor2.getId(), 3, "Discussion", "SQL review", dailyEntry3ParentIds);
		
		tutor2.executeCommand(co, "Add", timecard3, timecard3.getParentIds());
		tutor2.executeCommand(co, "Add", timecard4, timecard4.getParentIds());
		tutor2.executeCommand(co, "Add", classEntry31, classEntry31.getParentIds());
		tutor2.executeCommand(co, "Add", classEntry32, classEntry32.getParentIds());
		tutor2.executeCommand(co, "Add", dailyEntry311, dailyEntry311.getParentIds());
		tutor2.executeCommand(co, "Add", dailyEntry312, dailyEntry312.getParentIds());
		
		tutor2.executeCommand(co, "Remove", timecard3, timecard3.getParentIds());
		ArrayList<String> submittedId = new ArrayList<String>();
		submittedId.add("Submitted" + tutor2.getId());
		tutor2.executeCommand(co, "Add", timecard3, submittedId);
		professor2.executeCommand(co, "Add", classEntry31, new ArrayList<String>());
		professor2.executeCommand(co, "Add", classEntry32, new ArrayList<String>());
		
		// tutor2.executeCommand(co, "Submit", timecard3, timecard3.getParentIds());
		
		co.persist();
		tutor1.persist();
		tutor2.persist();
		tutor3.persist();
		professor1.persist();
		professor2.persist();
		professor3.persist();
		hr1.persist();
		hr2.persist();
		hr3.persist();
	}
}
